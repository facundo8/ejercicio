CREATE TABLE public.productos (
    id_producto serial NOT NULL,
    descripcion varchar NULL,
    precio_base numeric NULL,
    cantidad numeric NULL,
    CONSTRAINT productos_pk PRIMARY KEY (id_producto)
);
