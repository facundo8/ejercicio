<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-12 18:46:04
  from '/home/facu/Desktop/workspace/ejercicio/vista.productos.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e938c1c82dfc1_92323314',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '56a7099b06e680e6d30d09ae087bb415744c9de0' => 
    array (
      0 => '/home/facu/Desktop/workspace/ejercicio/vista.productos.tpl',
      1 => 1586727962,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e938c1c82dfc1_92323314 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/facu/Desktop/workspace/ejercicio/vendor/smarty/smarty/libs/plugins/modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),1=>array('file'=>'/home/facu/Desktop/workspace/ejercicio/vendor/smarty/smarty/libs/plugins/modifier.replace.php','function'=>'smarty_modifier_replace',),));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cabecera']->value, 'nombre');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['nombre']->value) {
?>
                    <th><?php echo smarty_modifier_replace(smarty_modifier_capitalize($_smarty_tpl->tpl_vars['nombre']->value),'_',' ');?>
</th>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productos']->value, 'producto');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['producto']->value) {
?>
              <tr>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['producto']->value, 'valor');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['valor']->value) {
?>
                    <td><?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
</td>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <td><a href="formularioEliminar.html">eliminar</a></td>
                <td><a href="formularioActualizacion.html">modificar</a></td>
              </tr>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
    </table>
    <br />
    <nav>
        <a href="/">Volver</a>
    </nav>
</body>
</html>
<?php }
}
